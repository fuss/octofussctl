#!/usr/bin/env python
from distutils.core import setup

setup(name='octofussctl',
      version='0.3.3',
      description='octofuss command line client',
      author=['Enrico Zini','Christopher Gabriel'],
      author_email=['enrico@truelite.it', 'cgabriel@truelite.it'],
      url='https://work.fuss.bz.it/projects/octofussctl',
      scripts=['octofussctl'],
     )
