#!/usr/bin/env python3

import octofuss
import getpass
import sys, os, os.path, re, time
import xmlrpc.client
import cmd
import fnmatch
import textwrap
import curses
import optparse
from octofuss.xmlrpc import APIKeyException
from json import loads

def command(fun):
    def handle(*args):
        try:
            try:
                fun(*args)
            except APIKeyException:
                print("[Server has restarted: logging in again]", file=sys.stderr)
                args[0]._relogin()
                fun(*args)
        except Exception as e:
            print("Error (%s): %s" % (e.__class__.__name__, e), file=sys.stderr)
    return handle

def maybe_json(value):
    value = value.strip()
    if value[0] == "{":
        return loads(value)
    return value

class Parser(optparse.OptionParser):
    def __init__(self, *args, **kw):
        optparse.OptionParser.__init__(self, usage=optparse.SUPPRESS_USAGE, add_help_option=False, *args, **kw)
    def error(self, msg):
        print("Error:", msg, file=sys.stderr)

class App(cmd.Cmd):
    def __init__(self, url, user, password):
        cmd.Cmd.__init__(self)
        curses.setupterm()

        # Set up special option parsers for our commands
        self.listParser = Parser()
        self.listParser.add_option("-l", "--long", action="store_true")

        self.url = url
        # Perform login
        self.loginServer = xmlrpc.client.ServerProxy(url)
        apikey = self.loginServer.login(user, password)
        if apikey == "":
            raise KeyError("login failed")
        self.cached_user = user
        self.cached_password = password
        self.tree = octofuss.xmlrpc.Client(url, apikey)
        self.intro = "Welcome to the octofussd client.  Queries are sent to " + url
        self.root = "/"
        self.prompt = self.root + "> "
        self.identchars = "".join(set(self.identchars) | set("/"))

    def _solve_relpath(self, path):
        return os.path.normpath(os.path.join(self.root, path))

    def _relogin(self):
        apikey = self.loginServer.login(self.cached_user, self.cached_password)
        if apikey == "":
            raise KeyError("login failed")
        self.tree = octofuss.xmlrpc.Client(self.url, apikey)

    def _complete_generic(self, text, line, begidx, endidx):
        if begidx > 0:
            sp = line.rfind(" ", 0, begidx)
            if sp == -1: sp = 0
            if begidx != sp:
                text = line[sp+1:begidx] + text
        root, part = os.path.split(text)
        try:
            root = self._solve_relpath(root)
            return [i for i in self.tree.list(root) if i.startswith(part)]
        except Exception as e:
            print("Cannot list %s: %s" % (root, e), file=sys.stderr)

    complete_cd = _complete_generic
    def help_cd(self):
        print("""Move to a different tree node

After moving to a tree node, all path given are relative to that node.

cd .. goes up one level, just like in the shell.

The current node is shown in the prompt.

Examples:
  /> cd /users/users/itelr
  /users/users/itelr> ls
    (will list the information for the user 'itelr')
  /users/users/itelr> cd ../silderm
  /users/users/silderm> cd /
  />
""")
    def do_cd(self, path):
        root = self._solve_relpath(path)
        if not self.tree.has(root):
            print("Invalid path:", path, file=sys.stderr)
        else:
            self.root = root
            self.prompt = self.root + "> "

    complete_has = _complete_generic
    def help_has(self):
        print("""Tests if a given tree node exists

Examples:
  /> has /
  True
  /> has /users
  True
  /> has /users/users/user-that-does-not-exist
  False
""")
    @command
    def do_has(self, path):
        path = self._solve_relpath(path)
        print(self.tree.has(path))

    complete_get = _complete_generic
    def help_get(self):
        print("""Get the value of a tree node

The result could be a string, a structure encoded in JSON format, or 'None' if
the node has no value.

get without parameters returns the value of the current node.

Examples:
  /> get /quota/root/user/itelr
  {u'file_hard': 200000, u'file_used': 119804, u'file_soft': 100000, u'disk_hard': 30000, u'disk_used': 13797, u'disk_soft': 20000}
  /> get /
  None
  /> cd /quota/root/user/itelr
  /quota/root/user/itelr> get
  {u'file_hard': 200000, u'file_used': 119804, u'file_soft': 100000, u'disk_hard': 30000, u'disk_used': 13797, u'disk_soft': 20000}
""")
    @command
    def do_get(self, path):
        path = self._solve_relpath(path)
        print(self.tree.get(path))

    complete_set = _complete_generic
    def help_set(self):
        print("""Set the value for a tree node

Examples:
  Set a tree node to the given value:

    /firewall/conf> set allowed_lan_hosts www.example.com

  If the value starts with a curly braces, it is parsed as a structure
  encoded in JSON format:

    /users/users> set itelr {"gecos": "Itel Rossari"}
""")
    @command
    def do_set(self, args):
        args = args.split(None, 1)
        path = args[0]
        if len(args) > 1:
            value = maybe_json(args[1])
        else:
            value = ""
        path = self._solve_relpath(path)
        self.tree.set(path, value)

    complete_list = _complete_generic
    def help_list(self):
        print("""List the children of a tree node

ls without parameters lists the children of the current node.

If list is given a value with asterisks, the result will be filtered like in
the shell.

With the option "-l", list also prints a short documentation for every child
node, if available.

Examples:
  /> ls
  contentfilter
  samba
  users
  firewall
  ...
  /> ls c*
  contentfilter
  computers
  cluster
  /> ls /samba
  cdrom
  print%24
  public
  /> ls /samba/p*
  print%24
  public
""")
    @command
    def do_list(self, args):
        (options, args) = self.listParser.parse_args(args=re.split(r"\s+", args))
        path = len(args) > 0 and args[0] or ""

        spath = path.rsplit("/", 1)
        if spath[-1].find("*") != -1:
            if len(spath) == 1:
                filter = spath[0]
                path = ""
            else:
                filter = spath[1]
                path = spath[0]
        else:
            filter = None
        path = self._solve_relpath(path)
        if options.long:
            start = time.time()
            try:
                width = curses.tigetnum("cols")
            except Exception as e:
                width = 80
            children = self.tree.list(path)
            children.sort()
            wrapper = textwrap.TextWrapper(width=width, replace_whitespace=False)
            csize = max([len(x) for x in children])
            toolong = False
            for c in children:
                wrapper.initial_indent = c.ljust(csize) + " "
                wrapper.subsequent_indent = " " * (csize + 1)
                if time.time() - start > 5:
                    d = None
                    toolong = True
                else:
                    d = self.tree.doc(path + "/" + c)
                if d:
                    d = d.split("\n", 1)[0]
                    print(wrapper.fill(d))
                else:
                    print(c)
            if toolong:
                print("The documentation of some items has been skipped to avoid you getting bored.", file=sys.stderr)
        else:
            for res in self.tree.list(path):
                if filter and not fnmatch.fnmatch(res, filter): continue
                print(res)

    complete_ls = _complete_generic
    help_ls = help_list
    do_ls = do_list

    complete_delete = _complete_generic
    def help_delete(self):
        print("""Delete a tree node

Examples:
  Delete a user:
    /users/users> has itelr
    True
    /users/users> delete itelr
    /users/users> has itelr
    False
""")
    @command
    def do_delete(self, path):
        path = self._solve_relpath(path)
        self.tree.delete(path)

    complete_create = _complete_generic
    def help_create(self):
        print("""Create a new node in the tree

Examples:
  Creates a tree node with the given value:

    /firewall/conf> create allowed_lan_hosts www.example.com

  Create the user named 'itelr' with all default values:

    /users/users> create itelr

  If the value starts with a curly braces, it is parsed as a structure
  encoded in JSON format:

    /users/users> create itelr {"gecos": "Itel Rossari"}
""")
    @command
    def do_create(self, args):
        args = args.split(None, 1)
        path = args[0]
        if len(args) > 1:
            value = maybe_json(args[1])
        else:
            value = ""
        path = self._solve_relpath(path)
        print(self.tree.create(path, value))

    complete_doc = _complete_generic
    def help_doc(self):
        print("""Get documentation for a tree node.

Examples:
  /> doc
    Prints the documentation for the current location
  /> doc users/groups/Domain+Admins
    Prints the documentation for the given tree location
""")
    @command
    def do_doc(self, path):
        try:
            width = curses.tigetnum("cols")
        except Exception as e:
            width = 80

        path = self._solve_relpath(path)
        doc = self.tree.doc(path)
        if doc:
            #print "Documentation for %s:" % path
            print(doc)
        else:
            print("(no documentation available)")
        children = self.tree.list(path)
        children.sort()
        maxchildren = 20
        oldlen = len(children)
        children = children[:maxchildren]
        if len(children) > 0:
            wrapper = textwrap.TextWrapper(width=width, replace_whitespace=False)
            print()
            if oldlen > maxchildren:
                print("Contains (first %d/%d elements):" % (maxchildren, oldlen))
            else:
                print("Contains:")
            csize = max([len(x) for x in children])
            for c in children:
                wrapper.initial_indent = "  " + c.rjust(csize) + " "
                wrapper.subsequent_indent = " " * (csize + 3)
                d = self.tree.doc(path + "/" + c)
                if d:
                    d = d.split("\n", 1)[0]
                else:
                    d = "(no documentation available)"
                print(wrapper.fill(d))

    def help_quit(self):
        print("Exit the program")
    def do_quit(self, *args):
        return True

    def emptyline(self):
        pass

    def default(self, cmd, *args):
        if cmd == "EOF":
            print()
            return True
        print("Invalid command:", cmd, " ".join(args), file=sys.stderr)


class MainParser(optparse.OptionParser):
    def __init__(self, *args, **kw):
        optparse.OptionParser.__init__(self, *args, **kw)
    def error(self, msg):
        sys.stderr.write("%s: error: %s\n\n" % (self.get_prog_name(), msg))
        self.print_help(sys.stderr)
        sys.exit(2)

oparser = MainParser(usage="[options] URL [command [args...]]")
oparser.add_option("-u", "--user", action="store", help="user name for connecting to the server")
oparser.add_option("-b", "--batch", metavar="BATCHFILE", help="file from where to read commands")
(opts, args) = oparser.parse_args()

if not args:
    oparser.error("Please provide the server URL")

url = args[0]

user = os.environ.get("OCTOFUSS_USER", None)
if opts.user is not None:
    user = opts.user
if user is None:
    user = input("Username: ")

password = os.environ.get("OCTOFUSS_PASSWORD", None)
if password is None:
    password = getpass.getpass("Password: ")
try:
    app = App(url, user, password)
except Exception as e:
    print(e)
    sys.exit(2)

if opts.batch:
    try:
        batchlines = open(opts.batch).readlines()
        for line in batchlines:
            if line.startswith("#") or line.startswith(" "):
                continue
            s = line.split()
            if len(s) < 2:
                print("Invalid line:", line, file=sys.stderr)
                continue
            cmd = s[0]
            func = getattr(app, "do_" + cmd)
            if func == None:
                print("Invalid command:", cmd, file=sys.stderr)
            func(" ".join(s[1:]))

    except Exception as e:
        print("Invalid batch file:", e, file=sys.stderr)

elif len(args) > 2:
    cmd = args[1]
    func = getattr(app, "do_" + cmd)
    if func == None:
        print("Invalid command:", cmd, file=sys.stderr)
    func(" ".join(args[2:]))
else:
    try:
        app.cmdloop()
    except KeyboardInterrupt:
        print()
        pass
    except KeyError as e:
        print(e)
